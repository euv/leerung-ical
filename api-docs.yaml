openapi: 3.0.1
info:
  title: Leerungsabfrage REST API
  description: API für die Abfrage von Leerungen an Häusern in Castrop-Rauxel
  termsOfService: http://swagger.io/terms/
  license:
    name: Apache 2.0
    url: http://springdoc.org
  version: 0.6.5
servers:
- url: https://api.euv-stadtbetrieb.de/leerung/
tags:
- name: Straßenverzeichnis
  description: API für die Abfrage des Straßenverzeichnis
- name: Leerungen
  description: Abfrage der Leerungen für Häuser
- name: leerung
  description: Abfrage der Leerungen für Häuser
- name: strasse
- name: hausnummer
  description: API für die Abfrage von Häusern und Hausnummern
- name: Hausnummern
  description: API für die Abfrage von Häusern und Hausnummern
paths:
  /v3/strasse:
    get:
      tags:
      - Straßenverzeichnis
      summary: Liste aller Straßen im Stadtgebiet
      description: Die Abfrage kann über verschiedene Parameter eingegenzt werden.
      operationId: listStrassen_1
      parameters:
      - name: size
        in: query
        description: Die Anzahl der zurückgegebenden Ergebnisse
        required: false
        schema:
          type: integer
          format: int32
      - name: search
        in: query
        description: Ein Suchstring nach dem gesucht werden soll
        required: false
        schema:
          type: string
          default: ""
      - name: part
        in: query
        description: Soll als Teilstring gesucht werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Strasse'
        "404":
          description: Ein Fehler mit der Datenbank ist aufgetretten
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
  /v3/strasse/{stId}:
    get:
      tags:
      - Straßenverzeichnis
      summary: Informationen zu einer Straßen ID abrufen
      operationId: getStrasseById_1
      parameters:
      - name: stId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Strasse'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Strasse'
        "404":
          description: Eine Straße mit dieser ID ist nicht vorhanden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Strasse'
  /v3/leerung/{haId}:
    get:
      tags:
      - Leerungen
      summary: Liste der Leerungen für die Hausnummer
      description: Frage alle Leerungen für eine Haus/Hausnummer ab
      operationId: getLeerungByHaId_2
      parameters:
      - name: haId
        in: path
        description: Die Haus ID
        required: true
        schema:
          type: integer
          format: int64
      - name: full
        in: query
        description: Sollen auch die Werte für die Straße gefüllt werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLocalDate'
        "404":
          description: Für diese Hausnummer wurden keine Leerungen gefunden.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLocalDate'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLocalDate'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLocalDate'
  /v3/leerung/feiertagsverschiebung/{haId}:
    get:
      tags:
      - Leerungen
      summary: Liste der Leerungen für die Hausnummer mit Feiertagsverschiebung
      description: Erweiter die Information um die Angaben bei der Leerung ob diese
        von einem Tag verschoben wurde und auf welchen
      operationId: getLeerungFeiertagsverschiebungByHaId_2
      parameters:
      - name: haId
        in: path
        description: Die Haus ID
        required: true
        schema:
          type: integer
          format: int64
      - name: full
        in: query
        description: Sollen auch die Werte für die Straße gefüllt werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
        "404":
          description: Für diese Hausnummer wurden keine Leerungen gefunden.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
  /v3/haus:
    get:
      tags:
      - Hausnummern
      summary: Liste aller Häuser/Hausnummern auf einer Straße
      description: Listet alle Häuser mit Hausnummern auf einer Straße auf
      operationId: listAllHausByStId_1
      parameters:
      - name: stId
        in: query
        description: Die Straßen ID
        required: true
        schema:
          type: integer
          format: int64
      - name: full
        in: query
        description: Sollen die Straßenwerte mit zurück gegeben werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
        "404":
          description: Auf der Straße wurden keine Hausnummern gefunden.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
  /v3/haus/{haId}:
    get:
      tags:
      - Hausnummern
      summary: Gibt die Informationen zu einem Haus/Hausnummer zurück
      operationId: getHausById_1
      parameters:
      - name: haId
        in: path
        description: Die Haus ID
        required: true
        schema:
          type: integer
          format: int64
      - name: full
        in: query
        description: Sollen die Straßenwerte mit zurück gegeben werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Abfrage erfolgreich
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
        "404":
          description: Die Hausnummer wurde nicht gefunden.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
  /v1/strasse/list:
    get:
      tags:
      - strasse
      summary: Liste aller Straßen im Stadtgebiet
      description: Die Abfrage kann über verschiedene Parameter eingegenzt werden.
      operationId: list_2
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Strasse'
        "404":
          description: Ein Fehler mit der Datenbank ist aufgetretten
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
      deprecated: true
  /v1/strasse/list/{strasse}:
    get:
      tags:
      - strasse
      summary: Listet alle Straßen mit entsprechenden Anfang auf
      description: Die Abfrage kann über verschiedene Parameter eingegenzt werden.
      operationId: listByName_1
      parameters:
      - name: strasse
        in: path
        description: Anfangsbuchstaben des Straßennamens
        required: true
        schema:
          type: string
      - name: size
        in: query
        description: Die maximale Anzahl der Treffer
        required: false
        schema:
          type: integer
          format: int32
          default: 10
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Strasse'
        "404":
          description: Es wurden keine Straßen gefunden die auf den Filter passen
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
      deprecated: true
  /v1/strasse/list-part/{strasse}:
    get:
      tags:
      - strasse
      summary: Listet alle Straßen mit bestand der Anfrage auf
      description: Die Abfrage kann über verschiedene Parameter eingegenzt werden.
      operationId: listByPartName_1
      parameters:
      - name: strasse
        in: path
        description: Zeichenkette im Straßenname
        required: true
        schema:
          type: string
      - name: size
        in: query
        description: Die maximale Anzahl der Treffer
        required: false
        schema:
          type: integer
          format: int32
          default: 10
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Strasse'
        "404":
          description: Ein Fehler mit der Datenbank ist aufgetretten
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                oneOf:
                - type: array
                  items:
                    $ref: '#/components/schemas/Strasse'
                - $ref: '#/components/schemas/ErrorEntity'
      deprecated: true
  /v1/strasse/get/{stId}:
    get:
      tags:
      - strasse
      summary: Informationen zu einer Straßen ID abrufen
      operationId: getId_1
      parameters:
      - name: stId
        in: path
        description: Die Straßen ID
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Strasse'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Strasse'
        "404":
          description: Eine Straße mit dieser ID ist nicht vorhanden
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Strasse'
      deprecated: true
  /v1/leerung/get/{haId}:
    get:
      tags:
      - leerung
      summary: Liste der Leerungen für die Hausnummer
      description: Frage alle Leerungen für eine Haus/Hausnummer ab
      operationId: getLeerungByHaId_1_1
      parameters:
      - name: haId
        in: path
        description: Die Haus ID
        required: true
        schema:
          type: integer
          format: int64
      - name: full
        in: query
        description: Sollen auch die Werte für die Straße gefüllt werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericDate'
        "404":
          description: Für diese Hausnummer wurden keine Leerungen gefunden.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericDate'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericDate'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericDate'
      deprecated: true
  /v1/leerung/get/feiertagsverschiebung/{haId}:
    get:
      tags:
      - leerung
      summary: Liste der Leerungen für die Hausnummer mit Feiertagsverschiebung
      description: Erweiter die Information um die Angaben bei der Leerung ob diese
        von einem Tag verschoben wurde und auf welchen
      operationId: getLeerungFeiertagsverschiebungByHaId_1_1
      parameters:
      - name: haId
        in: path
        description: Die Haus ID
        required: true
        schema:
          type: integer
          format: int64
      - name: full
        in: query
        description: Sollen auch die Werte für die Straße gefüllt werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
        "404":
          description: Für diese Hausnummer wurden keine Leerungen gefunden.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LeerungenGenericLeerungsDate'
      deprecated: true
  /v1/haus/list/{stId}:
    get:
      tags:
      - hausnummer
      summary: Liste aller Häuser/Hausnummern auf einer Straße
      description: Listet alle Häuser mit Hausnummern auf einer Straße auf
      operationId: list_1_1
      parameters:
      - name: stId
        in: path
        description: Die Straßen ID
        required: true
        schema:
          type: integer
          format: int64
      - name: strasse
        in: query
        description: Sollen die Straßenwerte mit zurück gegeben werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Erfolgreiche Abfrage
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
        "404":
          description: Auf der Straße wurden keine Hausnummern gefunden.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hausnummer'
      deprecated: true
  /v1/haus/get/{haId}:
    get:
      tags:
      - hausnummer
      summary: Gibt die Informationen zu einem Haus/Hausnummer zurück
      operationId: getById_1
      parameters:
      - name: haId
        in: path
        description: Die Haus ID
        required: true
        schema:
          type: integer
          format: int64
      - name: strasse
        in: query
        description: Sollen die Straßenwerte mit zurück gegeben werden
        required: false
        schema:
          type: boolean
          default: false
      responses:
        "200":
          description: Abfrage erfolgreich
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
        "404":
          description: Die Hausnummer wurde nicht gefunden.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
        "400":
          description: Abfrage war nicht korrekt
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
        "500":
          description: Ein Fehler mit der Datenbank ist aufgetretten.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hausnummer'
      deprecated: true
components:
  schemas:
    ErrorEntity:
      type: object
      properties:
        code:
          type: integer
          description: Der interne Fehlercode
          format: int32
        message:
          type: string
          description: Die Fehlermeldung
      description: Eine Fehlermeldung mit Fehlergrund
    Strasse:
      required:
      - id
      - name
      type: object
      properties:
        id:
          type: integer
          description: Die Straßen ID
          format: int64
        name:
          type: string
          description: Der Straßenname
      description: Information zu einer Straße
    LeerungenGenericLocalDate:
      required:
      - blau
      - braun
      - gelb
      - grau
      type: object
      properties:
        haId:
          type: integer
          description: Die Haus ID
          format: int64
        stId:
          type: integer
          description: Die Straßen ID
          format: int64
        strasse:
          type: string
          description: Der Straßenname
        nummer:
          type: string
          description: Die Hausnummer
        gelb:
          type: array
          description: Array mit den Leerung für Wertstoffe
          items:
            type: string
            description: Array mit den Leerung für Wertstoffe
            format: date
        braun:
          type: array
          description: Array mit den Leerung für die Biotonne
          items:
            type: string
            description: Array mit den Leerung für die Biotonne
            format: date
        grau:
          type: array
          description: Array mit den Leerung für Restmüll
          items:
            type: string
            description: Array mit den Leerung für Restmüll
            format: date
        blau:
          type: array
          description: Array mit den Leerung für die Papiertonne
          items:
            type: string
            description: Array mit den Leerung für die Papiertonne
            format: date
      description: Information zu Leerungen für ein Haus
    LeerungenGenericLeerungsDate:
      required:
      - blau
      - braun
      - gelb
      - grau
      type: object
      properties:
        haId:
          type: integer
          description: Die Haus ID
          format: int64
        stId:
          type: integer
          description: Die Straßen ID
          format: int64
        strasse:
          type: string
          description: Der Straßenname
        nummer:
          type: string
          description: Die Hausnummer
        gelb:
          type: array
          description: Array mit den Leerung für Wertstoffe
          items:
            $ref: '#/components/schemas/LeerungsDate'
        braun:
          type: array
          description: Array mit den Leerung für die Biotonne
          items:
            $ref: '#/components/schemas/LeerungsDate'
        grau:
          type: array
          description: Array mit den Leerung für Restmüll
          items:
            $ref: '#/components/schemas/LeerungsDate'
        blau:
          type: array
          description: Array mit den Leerung für die Papiertonne
          items:
            $ref: '#/components/schemas/LeerungsDate'
      description: Information zu Leerungen für ein Haus
    LeerungsDate:
      required:
      - leerung
      - verschoben
      type: object
      properties:
        verschoben:
          type: boolean
          description: Ob die Leerung an einen anderen Tag verschoben wurde
        leerung:
          type: string
          description: Das Datum der Leerung
          format: date
        orgLeerung:
          type: string
          description: Das Datum der Leerung wenn diese nicht verschoben wäre
          format: date
      description: Informationen über die Leerung
    Hausnummer:
      required:
      - id
      - nummer
      type: object
      properties:
        id:
          type: integer
          description: Die ID des Hauses
          format: int64
        stId:
          type: integer
          description: Die ID der Straße
          format: int64
        strasse:
          type: string
          description: Der Straßenname
        nummer:
          type: string
          description: Die Hausnummer
      description: Information zu einem Haus mit entsprechender Hausnummer
    LeerungenGenericDate:
      required:
      - blau
      - braun
      - gelb
      - grau
      type: object
      properties:
        haId:
          type: integer
          description: Die Haus ID
          format: int64
        stId:
          type: integer
          description: Die Straßen ID
          format: int64
        strasse:
          type: string
          description: Der Straßenname
        nummer:
          type: string
          description: Die Hausnummer
        gelb:
          type: array
          description: Array mit den Leerung für Wertstoffe
          items:
            type: string
            description: Array mit den Leerung für Wertstoffe
            format: date-time
        braun:
          type: array
          description: Array mit den Leerung für die Biotonne
          items:
            type: string
            description: Array mit den Leerung für die Biotonne
            format: date-time
        grau:
          type: array
          description: Array mit den Leerung für Restmüll
          items:
            type: string
            description: Array mit den Leerung für Restmüll
            format: date-time
        blau:
          type: array
          description: Array mit den Leerung für die Papiertonne
          items:
            type: string
            description: Array mit den Leerung für die Papiertonne
            format: date-time
      description: Information zu Leerungen für ein Haus
