# iCal interface from EUV
The iCal interface communicate with an other webservice in the background.
The data will translate to the webcal protocol.

# Calling the webservice
The production part is located under:

http://api.euv-stadtbetrieb.de/leerung-ical/v1/{ID}

where ID is the id of the house in our system. Each house has a unique id which only change when something
happen with the owner, eg. sold.

Extra parameters are accepted by the interface:

|-----------|--------------------------------------------------------------------------|
| Parameter | Description                                                              |
|-----------|--------------------------------------------------------------------------|
| alarm     | To each entry an alarm event is added. The value how many hours before midnight. Eg. 6 means 18 (24-6). [alarm=6] |
| link      | The entries are only for the next 3 month, not more. [link=true]         |
| series    | The entries are exported as a series not as single events. [series=true] |
|-----------|--------------------------------------------------------------------------|

# Contacts
marcus.schneider@euv-stadtbetrieb.de
