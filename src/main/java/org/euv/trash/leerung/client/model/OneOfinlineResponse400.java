/*
 * Leerungsabfrage REST API
 * API für die Abfrage von Leerungen an Häusern in Castrop-Rauxel
 *
 * OpenAPI spec version: 0.2.1-oas3
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.euv.trash.leerung.client.model;

/**
* OneOfinlineResponse400
*/
public interface OneOfinlineResponse400 {

}
