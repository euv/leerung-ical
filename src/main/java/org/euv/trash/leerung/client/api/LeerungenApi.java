package org.euv.trash.leerung.client.api;

import org.euv.trash.leerung.client.invoker.ApiClient;

import org.euv.trash.leerung.client.model.LeerungenGenericLeerungsDate;
import org.euv.trash.leerung.client.model.LeerungenGenericLocalDate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-12-14T08:17:54.663Z[GMT]")@Component("org.euv.trash.leerung.client.api.LeerungenApi")
public class LeerungenApi {
    private ApiClient apiClient;

    public LeerungenApi() {
        this(new ApiClient());
    }

    @Autowired
    public LeerungenApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Liste der Leerungen für die Hausnummer
     * Frage alle Leerungen für eine Haus/Hausnummer ab
     * <p><b>200</b> - Erfolgreiche Abfrage
     * <p><b>400</b> - Abfrage war nicht korrekt
     * <p><b>404</b> - Für diese Hausnummer wurden keine Leerungen gefunden.
     * <p><b>500</b> - Ein Fehler mit der Datenbank ist aufgetretten.
     * @param haId Die Haus ID
     * @param full Sollen auch die Werte für die Straße gefüllt werden
     * @return LeerungenGenericLocalDate
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public LeerungenGenericLocalDate getLeerungByHaId2(Long haId, Boolean full) throws RestClientException {
        Object postBody = null;
        // verify the required parameter 'haId' is set
        if (haId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'haId' when calling getLeerungByHaId2");
        }
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("haId", haId);
        String path = UriComponentsBuilder.fromPath("/v3/leerung/{haId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "full", full));

        final String[] accepts = { 
            "application/json"
         };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<LeerungenGenericLocalDate> returnType = new ParameterizedTypeReference<LeerungenGenericLocalDate>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Liste der Leerungen für die Hausnummer mit Feiertagsverschiebung
     * Erweiter die Information um die Angaben bei der Leerung ob diese von einem Tag verschoben wurde und auf welchen
     * <p><b>200</b> - Erfolgreiche Abfrage
     * <p><b>400</b> - Abfrage war nicht korrekt
     * <p><b>404</b> - Für diese Hausnummer wurden keine Leerungen gefunden.
     * <p><b>500</b> - Ein Fehler mit der Datenbank ist aufgetretten.
     * @param haId Die Haus ID
     * @param full Sollen auch die Werte für die Straße gefüllt werden
     * @return LeerungenGenericLeerungsDate
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public LeerungenGenericLeerungsDate getLeerungFeiertagsverschiebungByHaId2(Long haId, Boolean full) throws RestClientException {
        Object postBody = null;
        // verify the required parameter 'haId' is set
        if (haId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'haId' when calling getLeerungFeiertagsverschiebungByHaId2");
        }
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("haId", haId);
        String path = UriComponentsBuilder.fromPath("/v3/leerung/feiertagsverschiebung/{haId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "full", full));

        final String[] accepts = { 
            "application/json"
         };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<LeerungenGenericLeerungsDate> returnType = new ParameterizedTypeReference<LeerungenGenericLeerungsDate>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
