package org.euv.trash.leerung.client.invoker.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}