package org.euv.trash.ical.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.UUID;

import jakarta.servlet.http.HttpServletRequest;

import org.euv.trash.ical.PiwikService;
import org.euv.trash.leerung.client.api.LeerungenApi;
import org.euv.trash.leerung.client.invoker.ApiClient;
import org.euv.trash.leerung.client.invoker.RFC3339DateFormat;
import org.euv.trash.leerung.client.model.LeerungenGenericLeerungsDate;
import org.euv.trash.leerung.client.model.LeerungsDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.DateList;
import net.fortuna.ical4j.model.component.VAlarm;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.parameter.Value;
import net.fortuna.ical4j.model.property.Action;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.Name;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.RDate;
import net.fortuna.ical4j.model.property.Sequence;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Transp;
import net.fortuna.ical4j.model.property.TzId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.Url;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.model.property.XProperty;

@Slf4j
@RestController
public class IcalController {
	
	private final String HOMEPAGE_URL="https://www.euv-stadtbetrieb.de";
	private final String CALENDAR_NAME = "EUV Leerungstermine";
	private final String CALENDAR_COLOR = "#ffed00";
	
	@Autowired(required = false)
	BuildProperties buildProperties;
	
	@Autowired
	PiwikService piwikService;
	
	int aSequence;

	@GetMapping("/v1/{haId}")
	public ResponseEntity<Resource> generateIcalFile(
			@PathVariable(value = "haId", required = true)long haId,
			@RequestParam("jahr")Optional<Integer> reqJahr,
			@RequestParam(value = "link", defaultValue = "false")Optional<Boolean> link,
			@RequestParam(value = "series", defaultValue = "false")Optional<Boolean> series,
			@RequestParam(value = "alarm", defaultValue = "0")Optional<Integer> alarm,
			HttpServletRequest request) {
		long startTime = System.currentTimeMillis();
		String userInfo = null;
		try {
			LocalDate beginFilter, endFilter;
			if(link.isPresent() && link.get()) {
				beginFilter = LocalDate.now();
				endFilter = LocalDate.now().plusMonths(3);
			} else {
				if(reqJahr.isEmpty()) {
					beginFilter = LocalDate.now().withDayOfYear(1);
				} else {
					beginFilter = LocalDate.of(reqJahr.get(), 1, 1);
				}
				endFilter = beginFilter.plusMonths(12);
			}
			
			log.debug("Filter to year {} - {}", beginFilter, endFilter);
			
	
			LeerungenGenericLeerungsDate leerungen = getDataFromWebservice(haId);
			Calendar ical = new Calendar();
			initIcalCalendar(ical);
	
			String ort;
			ort = leerungen.getStrasse() +" "+ leerungen.getNummer();
			log.debug("Ort: {}", ort);
			userInfo = ort;
	
			aSequence = 0;
			if(series.isPresent() && series.get()) {
				/*
				 * Generate as series element
				 */
				if(!leerungen.getGrau().isEmpty())
					ical.getComponents().add(createIcalEvents(beginFilter, endFilter, leerungen.getGrau(), "Abfuhr Restabfall", ort, alarm));
				if(!leerungen.getGelb().isEmpty())
					ical.getComponents().add(createIcalEvents(beginFilter, endFilter, leerungen.getGelb(), "Abfuhr Wertstoff", ort, alarm));
				if(!leerungen.getBraun().isEmpty())
					ical.getComponents().add(createIcalEvents(beginFilter, endFilter, leerungen.getBraun(), "Abfuhr Bioabfall", ort, alarm));
				if(!leerungen.getBlau().isEmpty())
					ical.getComponents().add(createIcalEvents(beginFilter, endFilter, leerungen.getBlau(), "Abfuhr Altpapier", ort, alarm));
			} else {
				/*
				 * Generate a separate events
				 */
				leerungen.getGrau().stream()
					.filter(item -> item.getLeerung().compareTo(beginFilter)>=0 && item.getLeerung().compareTo(endFilter)<0)
					.forEach(item -> ical.getComponents().add(createIcalEvent(item, "Abfuhr Restabfall", ort, alarm)));
				leerungen.getGelb().stream()
					.filter(item -> item.getLeerung().compareTo(beginFilter)>=0 && item.getLeerung().compareTo(endFilter)<0)
					.forEach(item -> ical.getComponents().add(createIcalEvent(item, "Abfuhr Wertstoff", ort, alarm)));
				leerungen.getBraun().stream()
					.filter(item -> item.getLeerung().compareTo(beginFilter)>=0 && item.getLeerung().compareTo(endFilter)<0)
					.forEach(item -> ical.getComponents().add(createIcalEvent(item, "Abfuhr Bioabfall", ort, alarm)));
				leerungen.getBlau().stream()
					.filter(item -> item.getLeerung().compareTo(beginFilter)>=0 && item.getLeerung().compareTo(endFilter)<0)
					.forEach(item -> ical.getComponents().add(createIcalEvent(item, "Abfuhr Altpapier", ort, alarm)));
			}

			long time = System.currentTimeMillis() - startTime;
			piwikService.logCreateIcal(haId, userInfo, reqJahr, link, series, alarm, time, request);
			
			return createResponse(ical);
		}
		catch(Exception e) {
			long time = System.currentTimeMillis() - startTime;
			piwikService.logError(haId, reqJahr, link, series, alarm, e, time, request);
			return null;
		}
	}

	
	private VEvent createIcalEvents(LocalDate beginFilter, LocalDate endFilter, List<LeerungsDate> datums, String summary, String ort, Optional<Integer> alarm) {
		boolean isFirst = true;
		
		VEvent vevent = new VEvent();
		DateList dateList = new DateList();
		for(LeerungsDate datum : datums) {
			if(datum.getLeerung().compareTo(beginFilter)>=0 && datum.getLeerung().compareTo(endFilter)<0) {
				if(isFirst) {
					DtStart dtStart = new DtStart(toIcalDate(datum.getLeerung()));
					dtStart.getParameters().add(Value.DATE);
					vevent.getProperties().add(dtStart);
					vevent.getProperties().add(new Summary(summary));
					vevent.getProperties().add(new Location(ort));
			
					vevent.getProperties().add(new Description("EUV Stadtbetrieb Castrop-Rauxel\nWestring 215\n44575 Castrop-Rauxel"));
					vevent.getProperties().add(new Sequence(aSequence++));
					vevent.getProperties().add(new Transp("TRANSPARENT"));
			
					String forUuid = datum.getLeerung().toString() + ort + summary;
					UUID uuid = UUID.nameUUIDFromBytes(forUuid.getBytes());
					vevent.getProperties().add(new Uid(uuid.toString()));
	
					try {
						vevent.getProperties().add(new Url(new URI(HOMEPAGE_URL)));
					} catch(URISyntaxException e) {
						log.error("URL not converted");
					}
					isFirst = false;
				} else {
					dateList.add(toIcalDate(datum.getLeerung()));
				}
			}
		}
		
		RDate rdate = new RDate(dateList);
		rdate.getParameters().add(Value.DATE);
		vevent.getProperties().add(rdate);
		
		if(alarm.isPresent()) {
			VAlarm valarm = new VAlarm(Duration.ofHours(-alarm.get()));
			valarm.getProperties().add(Action.DISPLAY);
			valarm.getProperties().add(new Description("Erinnerung "+ summary));
			vevent.getAlarms().add(valarm);
		}
		
		return vevent;
	}

	private VEvent createIcalEvent(LeerungsDate datum, String summary, String ort, Optional<Integer> alarm) {
		
		VEvent vevent = new VEvent();
		DtStart dtStart = new DtStart(toIcalDate(datum.getLeerung()));
		dtStart.getParameters().add(Value.DATE);
		vevent.getProperties().add(dtStart);
		vevent.getProperties().add(new Summary(summary));
		vevent.getProperties().add(new Location(ort));

		vevent.getProperties().add(new Description("EUV Stadtbetrieb Castrop-Rauxel\nWestring 215\n44575 Castrop-Rauxel"));
		vevent.getProperties().add(new Sequence(aSequence++));
		vevent.getProperties().add(new Transp("TRANSPARENT"));

		String forUuid = datum.getLeerung().toString() + ort + summary;
		UUID uuid = UUID.nameUUIDFromBytes(forUuid.getBytes());
		vevent.getProperties().add(new Uid(uuid.toString()));

		try {
			vevent.getProperties().add(new Url(new URI(HOMEPAGE_URL)));
		} catch(URISyntaxException e) {
			log.error("URL not converted");
		}
		
		if(alarm.isPresent()) {
			VAlarm valarm = new VAlarm(Duration.ofHours(-alarm.get()));
			valarm.getProperties().add(Action.DISPLAY);
			valarm.getProperties().add(new Description("Erinnerung "+ summary));
			//String forAlarmUuid = "A+"+ forUuid;
			String forXAlarmUuid = "XA+"+ forUuid;
			valarm.getProperties().add(new Uid(UUID.nameUUIDFromBytes(forXAlarmUuid.getBytes()).toString()));
			valarm.getProperties().add(new XProperty("X-WR-ALARMUID", UUID.nameUUIDFromBytes(forXAlarmUuid.getBytes()).toString()));
			vevent.getAlarms().add(valarm);
		}
		
		return vevent;
	}

	private ResponseEntity<Resource> createResponse(Calendar calendar) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=ical.ics");
		headers.add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
		headers.add(HttpHeaders.PRAGMA, "no-cache");
		headers.add(HttpHeaders.EXPIRES, "0");
		if(buildProperties == null) {
			headers.add(HttpHeaders.CONTENT_TYPE, "text/text; charset=utf-8");
		} else {
			headers.add(HttpHeaders.CONTENT_TYPE, "text/calendar; charset=utf-8");
		}

		Resource body = new ByteArrayResource(calendar.toString().getBytes());
		
		return ResponseEntity.ok().headers(headers).body(body);
	}

	private void initIcalCalendar(Calendar ical) {
		ical.getProperties().add(new ProdId("-//EUV Stadtbetrieb Castrop-Rauxel -AöR-/Leerungen/DE"));
		ical.getProperties().add(Version.VERSION_2_0);
		ical.getProperties().add(CalScale.GREGORIAN);
		ical.getProperties().add(new TzId("Europe/Berlin"));
		ical.getProperties().add(new Name("EUV Abfallkalender"));
		ical.getProperties().add(new Summary("Leerung der Mülltonnen"));
		ical.getProperties().add(new XProperty("X-WR-CALNAME", CALENDAR_NAME));
		ical.getProperties().add(new XProperty("X-WR-CALDESC", CALENDAR_NAME));
		ical.getProperties().add(new XProperty("X-APPLE-CALENDAR-COLOR", CALENDAR_COLOR));
		ical.getProperties().add(new XProperty("X-OUTLOOK-COLOR", CALENDAR_COLOR));
		ical.getProperties().add(new XProperty("X-FUNAMBOL-COLOR", CALENDAR_COLOR));
		ical.getProperties().add(new XProperty("X-LOTUS-CHARSET", "UTF-8"));
	}

	private LeerungenGenericLeerungsDate getDataFromWebservice(long haId) {
		ApiClient apiClient = new ApiClient();
		apiClient.setDebugging(false);
		RFC3339DateFormat df = new RFC3339DateFormat();
		df.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
		apiClient.setDateFormat(df);

		try {
			LeerungenApi leerungenApi = new LeerungenApi(apiClient);
			LeerungenGenericLeerungsDate leerungen = leerungenApi.getLeerungFeiertagsverschiebungByHaId2(haId, true);
			return leerungen;
		}
		catch(Exception e) {
			log.error("Catch", e);
		}
		return null;
	}
	
	private Date toIcalDate(LocalDate datum) {
		return new Date(toDate(datum));
	}
	
	private java.util.Date toDate(LocalDate datum) {
		return java.util.Date.from(datum.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
}
