package org.euv.trash.ical;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class AppConfig {
	
	@Autowired(required = false)
	private BuildProperties buildProperties;
	
	@PostConstruct
	void printInfos() {
		if(buildProperties != null) {
			log.info("Name / Version: {} ({})", buildProperties.getName(), buildProperties.getVersion());
		} else {
			log.info("Name / Version: RUNNING IN DEV ENVIRONMENT");
		}
	}
}
